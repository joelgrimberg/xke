import type { LoaderArgs } from "@remix-run/node";
import { json } from "@remix-run/node";
import { useNavigate } from "@remix-run/react";
import { useEffect, useState } from "react";
import Header from "~/components/header";
import { getNoteListItems } from "~/models/note.server";
import { requireUserId } from "~/session.server";

export async function loader({ request }: LoaderArgs) {
  const userId = await requireUserId(request);
  const noteListItems = await getNoteListItems({ userId });
  return json({ noteListItems });
}

// Usage
function App() {
  function useKeyPress(targetKey: string) {
    // State for keeping track of whether key is pressed
    const [keyPressed, setKeyPressed] = useState<boolean>(false);

    // If pressed key is our target key then set to true

    // Add event listeners
    useEffect(() => {
      function downHandler({ key }: { key: string }) {
        if (key === "l") {
          navigate(`/`);
        }
        if (key === targetKey) {
          setKeyPressed(true);
        }
      }
      // If released key is our target key then set to false
      const upHandler = ({ key }: { key: string }) => {
        if (key === targetKey) {
          setKeyPressed(false);
        }
      };

      window.addEventListener("keydown", downHandler);
      window.addEventListener("keyup", upHandler);
      // Remove event listeners on cleanup
      return () => {
        window.removeEventListener("keydown", downHandler);
        window.removeEventListener("keyup", upHandler);
      };
    }, [targetKey]); // Empty array ensures that effect is only run on mount and unmount
    return keyPressed;
  }
  const navigate = useNavigate();

  // Call our hook for each key that we'd like to monitor
  const happyPress = useKeyPress("h");

  // handleKeyPress(target) {
  //   // I'm guessing you have value stored in state
  //   const { value } = this.state;
  //   if(target.charCode==13){
  //     navigate(`/search?q=${value}`);
  //   }
  // }
  /*
const navigate = useNavigate()
const handleClick = () => {
  navigate('.', { replace: true })
}
  */
  const sadPress = useKeyPress("s");
  const robotPress = useKeyPress("r");
  const foxPress = useKeyPress("f");
  return (
    <div>
      <div>h, s, r, f</div>
      <div>
        {happyPress && "😊"}
        {sadPress && "😢"}
        {robotPress && "🤖"}
        {foxPress && "🦊"}
      </div>
    </div>
  );
}
// Hook

export default function userPage() {
  return (
    <div>
      <Header page="Key" />
      <App />
    </div>
  );
}
